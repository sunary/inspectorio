import psycopg2
from datetime import datetime


def create_connection(database='Inspectorio', user='root', password='', host='localhost'):
    connection = psycopg2.connect(database=database, user=user, password=password, host=host)
    connection.set_client_encoding('utf-8')
    return connection


def create_table_from_dict(connection, table, sample_data):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM information_schema.tables WHERE table_name=%s", (table,))
    if cursor.rowcount:
        return

    str_create = gen_create_table_str(table, sample_data)
    cursor.execute(str_create)
    connection.commit()


def insert_dict(connection, table, input_data):
    cursor = connection.cursor()

    str_query, values = gen_insert_str(table, input_data)
    cursor.execute(str_query, tuple(values))
    connection.commit()


def gen_create_table_str(table, sample_data):
    keys, values = sample_data.keys(), sample_data.values()
    keys = list(keys)
    values = list(values)
    datatype = []

    for i in range(len(values)):
        if isinstance(values[i], bool):
            datatype.append('BOOLEAN')
        elif isinstance(values[i], int):
            datatype.append('INTEGER')
        elif isinstance(values[i], datetime):
            datatype.append('TIMESTAMP')
        else:
            datatype.append('VARCHAR(255)')

    zip_fields = zip(keys, datatype)
    return '''
CREATE TABLE {} (
    {}
)
'''.format(table, ', '.join(['{} {}'.format(x[0], x[1]) for x in zip_fields]))


def gen_insert_str(table, input_data):
    keys, values = input_data.keys(), input_data.values()
    keys = list(keys)
    values = list(values)

    return '''
INSERT INTO {}({})
VALUES ({})
'''.format(table, ', '.join(keys), ', '.join(['%s' for _ in range(len(keys))])), values
