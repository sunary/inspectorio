### Environment

+ Python 3.5
+ PostgreSQL client (default info: user: `'root'`, password: `''`, host: `'localhost'`)

### Install

`pip install -r requirements.txt`

### How to run

+ Main:

`python run.py filename.xlsx`

+ Test:

`python test.py`


