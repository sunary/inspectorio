import unittest
import data_access
import re


class MyTestCase(unittest.TestCase):

    def test_gen_create_table_string(self):
        expected_sql = '''
        CREATE TABLE table (
            string_field1 VARCHAR(255),
            string_field2 VARCHAR(255),
            int_field INTEGER,
            bool_field BOOLEAN
        )
        '''

        sample_data = {
            'string_field1': 'first',
            'string_field2': 'second',
            'int_field': 1,
            'bool_field': False
        }

        got_sql = data_access.gen_create_table_str('table', sample_data)
        self.assertEqual(self.normal_str(got_sql), self.normal_str(expected_sql))

    def test_gen_insert_table_string(self):
        expected_sql = '''
        INSERT INTO
        table(string_field1, string_field2, int_field, bool_field)
        VALUES (%s, %s, %s, %s)
        '''

        input_data = {
            'string_field1': 'first',
            'string_field2': 'second',
            'int_field': 1,
            'bool_field': False
        }

        got_sql, _ = data_access.gen_insert_str('table', input_data)
        self.assertEqual(self.normal_str(got_sql), self.normal_str(expected_sql))

    @staticmethod
    def normal_str(sql_str):
        sql_str = re.sub('\s+', ' ', sql_str)
        return sql_str.lower()


if __name__ == '__main__':
    unittest.main()
