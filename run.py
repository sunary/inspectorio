from openpyxl import load_workbook
import data_access

SHEET_INFO = [{
    'name': 'Inspection Detail',
    'table': ['general_info', 'pom_info']
    },
    {
    'name': 'PIF Detail - FRI',
    'table': ['pif_info', 'item_info', 'sip_element']
    }]


def read_worksheet(filename):
    wb = load_workbook(filename, data_only=True)
    sheet_name = wb.get_sheet_names()
    if len(sheet_name) == 2 and sheet_name[0] == SHEET_INFO[0]['name'] and sheet_name[1] == SHEET_INFO[1]['name']:
        return wb.get_sheet_by_name(sheet_name[0]), wb.get_sheet_by_name(sheet_name[1])

    return None, None


def extract_inspection_detail(worksheet):
    general_info = {
        'date': worksheet['B1'].value,
        'vendor': worksheet['B2'].value,
        'bpm_vendor': worksheet['H2'].value,
        'vdr_contacts': worksheet['B3'].value,
        'factor': worksheet['B4'].value,
        'fty_address': worksheet['B5'].value,
        'fty_contacts': worksheet['B6'].value,
        'auditor': worksheet['B7'].value,
        'frm_level': worksheet['F4'].value
    }

    pom_info = []

    for i in range(13, worksheet.max_row):
        if not worksheet['A{}'.format(i)].value:
            break

        info = {
            'pid': worksheet['A{}'.format(i)].value,
            'dpci': worksheet['B{}'.format(i)].value,
            'po_included': worksheet['C{}'.format(i)].value,
            'insp_type': worksheet['D{}'.format(i)].value,
            'po_qty': int(worksheet['E{}'.format(i)].value),
            'available_qty': int(worksheet['F{}'.format(i)].value),
            'description': worksheet['G{}'.format(i)].value,
            'is_pwi': True if worksheet['H{}'.format(i)].value.lower() == 'yes' else False,
        }
        pom_info.append(info)

    return general_info, pom_info


def extract_pif_detail(worksheet):
    pif_info = []
    for i in range(4, worksheet.max_row):
        if not worksheet['A{}'.format(i)].value:
            break

        info = {
            'vendor_style': worksheet['A{}'.format(i)].value,
            'po_number': worksheet['C{}'.format(i)].value,
            'purpose': worksheet['D{}'.format(i)].value,
            'ship_begin_date': worksheet['E{}'.format(i)].value,
            'ship_end_date': worksheet['F{}'.format(i)].value
        }

        pif_info.append(info)

    item_info = []
    for i in range(4, worksheet.max_row):
        if not worksheet['H{}'.format(i)].value:
            break

        info = {
            'item': worksheet['H{}'.format(i)].value,
            'item_description': worksheet['I{}'.format(i)].value,
            'po': worksheet['J{}'.format(i)].value,
            'order_qty': int(worksheet['K{}'.format(i)].value),
            'available_qty': int(worksheet['L{}'.format(i)].value),
            'vendor_style': worksheet['M{}'.format(i)].value,
            'assortment_item': worksheet['N{}'.format(i)].value
        }
        item_info.append(info)

    sip_element = []
    for i in range(4, worksheet.max_row):
        if not worksheet['V{}'.format(i)].value:
            break

        element = {
            'vendor': worksheet['V{}'.format(i)].value,
            'ppr_doc': worksheet['W{}'.format(i)].value,
            'red_seal_sample': worksheet['X{}'.format(i)].value,
            'technical_info': worksheet['Y{}'.format(i)].value,
            'final_item_setup_from': worksheet['Z{}'.format(i)].value,
            'total_program': worksheet['AA{}'.format(i)].value,
            'color_standards': worksheet['AB{}'.format(i)].value,
            'production_color': worksheet['AC{}'.format(i)].value,
            'trims_accessories_and_more': worksheet['AD{}'.format(i)].value,
            'yellow_seal_sample': worksheet['AE{}'.format(i)].value,
            'product_testing_result': worksheet['AF{}'.format(i)].value,
        }
        sip_element.append(element)

    return pif_info, item_info, sip_element


def main(args):
    if len(args) != 2:
        return

    filename = args[1]
    inspection_worksheet, pif_worksheet = read_worksheet(filename)

    if inspection_worksheet and pif_worksheet:
        connection = data_access.create_connection()

        inspection_data = extract_inspection_detail(inspection_worksheet)
        for i, table in enumerate(SHEET_INFO[0]['table']):
            if isinstance(inspection_data[i], dict):
                data_access.create_table_from_dict(connection, table, inspection_data[i])
                data_access.insert_dict(connection, table, inspection_data[i])
            elif isinstance(inspection_data[i], list):
                data_access.create_table_from_dict(connection, table, inspection_data[i][0])
                for elem in inspection_data[i]:
                    data_access.insert_dict(connection, table, elem)

        pif_data = extract_pif_detail(pif_worksheet)
        for i, table in enumerate(SHEET_INFO[1]['table']):
            if isinstance(pif_data[i], dict):
                data_access.create_table_from_dict(connection, table, pif_data[i])
                data_access.insert_dict(connection, table, pif_data[i])
            elif isinstance(pif_data[i], list):
                data_access.create_table_from_dict(connection, table, pif_data[i][0])
                for elem in pif_data[i]:
                    data_access.insert_dict(connection, table, elem)

        connection.close()


if __name__ == '__main__':
    import sys
    main(sys.argv)
